
#! /usr/bin/env python3

import sys
import csv
car_ds = list()
car_list = list()
#car_check = [1,2,3,60,90,300,500,639]
car_check = list(range(1, 640))
head = ('Car','Make','Model','Model ver','Years', 
        'Displacement','Power','Gas tank')
pads = {0:len(head[0]),
        1:len(head[1]),
        2:len(head[2]),
        3:len(head[3]),
        4:len(head[4]),
        5:len(head[5]),
        6:len(head[6]),
        7:len(head[7])}

class Car:
    """Common base class for all Cars from car_mod.py"""

    car_count = 0   

    def __init__(self, car, make, modelv, year):
        self.car = car
        self.make = make
        self.modelv = modelv
        self.year = year
        Car.car_count += 1
   
    def display_count(self):
        return (f"Total Cars: {Car.car_count}")

    def display_car(self):        
        str_ = (f"{self.car:<{pads[0]}} | "
                f"{self.make:<{pads[1]}} | "
                f"{self.modelv:<{pads[3]}} | "
                f"{self.year}")

        return (str_)


with open('car_models_dataset.csv', mode='r') as csvfile:
    cars = csv.reader(csvfile, delimiter=',')
    for c, row in enumerate(cars):
        if (c in car_check):
            list_ = [f'{c}']
            list_.extend(row)
            car_ds.append(list_)

for x in car_ds:
    for y in list(pads.keys()):
        if (len(x[y]) > pads[y]):
            pads[y] = len(x[y])

#print(f"{car_ds}\n# of items: {len(car_ds)}"); exit()

for x in car_ds:
    car_list.append(Car(x[0],x[1],x[3],x[4]))

#print(f"{car_list}\n# of items: {len(car_list)}"); exit()


print (f'__doc__    : {Car.__doc__}\n')

# Print current ID of Car instance different memory location than ex4.1.1.py
print(f"Current instance ID 'Car' from car_mod.py {id(Car)}")
# Accessing class built-in attributes
print (f'{Car.__dict__}\n') # Dictionary of class namespace
print (f'{Car.__doc__}\n') # Class docstring or none
print (f'{Car.__name__}\n') # Class name
print (f'{Car.__module__}\n') # Module name containing the class
print (f'{Car.__bases__}\n') # Tuple containing the base classes

# Changed Text to show this is coming from car_mod.py
# class Car:
# """Common base class for all Cars from car_mod.py"""

# End
