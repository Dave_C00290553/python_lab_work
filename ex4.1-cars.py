#! /usr/bin/env python3

"""  Exercise #4.1 in Programming II  """

import sys
import csv

car_ds = list()
car_list = list()
#car_check = [1,2,3,60,90,300,500,639]
car_check = list(range(1, 640))
head = ('Car','Make','Model','Model ver','Years', 
        'Displacement','Power','Gas tank')
pads = {0:len(head[0]),
        1:len(head[1]),
        2:len(head[2]),
        3:len(head[3]),
        4:len(head[4]),
        5:len(head[5]),
        6:len(head[6]),
        7:len(head[7])}

class Car:
    """Common base class for all Cars"""

    car_count = 0   

    def __init__(self, car, make, modelv, year):
        self.car = car
        self.make = make
        self.modelv = modelv
        self.year = year
        Car.car_count += 1
   
    def display_count(self):
        return (f"Total Cars: {Car.car_count}")

    def display_car(self):        
        str_ = (f"{self.car:<{pads[0]}} | "
                f"{self.make:<{pads[1]}} | "
                f"{self.modelv:<{pads[3]}} | "
                f"{self.year}")

        return (str_)

    # End of Car classs

with open('car_models_dataset.csv', mode='r') as csvfile:
    cars = csv.reader(csvfile, delimiter=',')
    for c, row in enumerate(cars):
        if (c in car_check):
            list_ = [f'{c}']
            list_.extend(row)
            car_ds.append(list_)

for x in car_ds:
    for y in list(pads.keys()):
        if (len(x[y]) > pads[y]):
            pads[y] = len(x[y])

#print(f"{car_ds}\n# of items: {len(car_ds)}"); exit()

for x in car_ds:
    car_list.append(Car(x[0],x[1],x[3],x[4]))

#print(f"{car_list}\n# of items: {len(car_list)}"); exit()


label = 'Number of Cars'
print(f'\n{label}')
print(f"{'-' *len(label)}\n")
print(f'Car count: {Car.car_count}')

label = 'All printed from within each instance'
print(f'\n{label}')
print(f"{'-' *len(label)}\n")

print(f"{head[0]:<{pads[0]}} " 
      f"| {head[1]:<{pads[1]}} "
      f"| {head[3]:<{pads[3]}} "
      f"| {head[4]}")

for x in [0,1,3]:
    print (f"{'-' * pads[x]} + ", end = '') 
print(f"{'-' * pads[4]}")

for c in list(range(0,len(car_list))):
    # Printing Car information from within each instance
    print(f'{car_list[c].display_car()}')

label = 'Changing attributes'
print(f'\n{label}')
print(f"{'-' *len(label)}\n")

print('modifying : Car 0 Years = 2016-2018') 
print('modifying : Car 1 Model = Kangoo') 
print(f'modifying : Car 2 Power = {car_ds[2][5]}\n') 

car_list[0].year = '2016-2018'
car_list[1].model = 'Kangoo'      
car_list[2].power = car_ds[2][5]           

for c, s in enumerate(car_list):
    try:
        print(f"'Car {c}  'power' value is {getattr(s, 'power')}")
    except:
        pass

label = 'Relooking at Car 2'
print(f'\n{label}')
print(f"{'-' *len(label)}\n")

print(f"{head[0]:<{pads[0]}} "
      f"| {head[1]:<{pads[1]}} "
      f"| {head[3]:<{pads[3]}} "
      f"| {head[4]:<{pads[4]}} "
      f"| {head[6]}")

for x in [0,1,3,4]:
    print (f"{'-' * pads[x]} + ", end = '') 
print(f"{'-' * pads[6]}")


print(car_list[2].display_car(), end = '')
print(f" | {car_list[2].power}\n")

print("Deleting the 'power' attribute for Car 2\n")
delattr(car_list[2], 'power')                 

print(f"Car 2 'power' value is {hasattr(car_list[2], 'power')}")

label = 'Accessing built-in attributes'
print(f'\n{label}')
print(f"{'-' *len(label)}\n")

print (f'__doc__    : {Car.__doc__}\n')    

print(f"Current instance ID 'Car' {id(Car)}")

# Accessing class built-in attributes
print (f'{Car.__dict__}\n') # Dictionary of class namespace
print (f'{Car.__doc__}\n') # Class docstring or none
print (f'{Car.__name__}\n') # Class name
print (f'{Car.__module__}\n') # Module name containing the class
print (f'{Car.__bases__}\n') # Tuple containing the base classes
# End
